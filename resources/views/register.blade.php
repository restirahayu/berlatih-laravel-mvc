<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome1" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="first"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="last"> <br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="2">Other <br><br>
        <label> Nationality:</label><br><br>
        <select>
            <option value="indonesia">Indonesian</option>
            <option value="usa">USA</option>
            <option value="korea">Korean</option>
            <option value="jerman">Germany</option>
            <option value="other">Other</option>
        </select>
        <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="bahasa" value="0">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="0">English <br>
        <input type="checkbox" name="bahasa" value="0">Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">


    </form>
</body>

</html>